﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {
	private bool playerInZone;
	
	public string LevelToLoad;
	// Use this for initialization
	void Start () {
		playerInZone = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.W) && playerInZone){

			Application.LoadLevel(LevelToLoad);
		}
	}
		void onTriggerEnter2D(Collider2D other)
		{
			if (other.name == "PlayerBoat") {
				playerInZone = true;
			}
			
		}

		void onTriggerExit2D(Collider2D other)
		{
			if (other.name == "PlayerBoat") {
				playerInZone = false;
			}
			
		}
}
