﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    
    public float guiPlacementY;
    public float guiPlacementY1;

    public void StartLevel() {
       Application.LoadLevel("Boat");
        
        
    }
    public Texture backgroundTexture;
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);
        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * guiPlacementY1, Screen.width * .5f, Screen.height * .1f), "Play again"))
        {
            
            StartLevel();
        }
     
        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * guiPlacementY, Screen.width * .5f, Screen.height * .1f), "Quit"))
        {
            Application.Quit();
        }
        
    }
}
