﻿using UnityEngine;
using System.Collections;

public class PlayerNermal : MonoBehaviour
{
	private Rigidbody2D myRigidbody;
	
	
	[SerializeField]
	private float movementSpeed;
	public bool grounded = true;
	public float JumpPower = 50f;
	public float jumpHeight = 10f;

	public int curHealth;
	public int maxHealth = 10;
	
	private int secondsUntilNextLevel = 1;
	private gameMaster gn;
	
	
	void Start ()
	{
		myRigidbody = GetComponent<Rigidbody2D> ();

		curHealth = maxHealth;
		gn = GameObject.FindGameObjectWithTag ("GameMaster").GetComponent<gameMaster> ();
	}
	
	void Update ()
	{
		float horizontal = Input.GetAxis ("Horizontal");
		HandleMovement(horizontal);
		
		if (Input.GetButtonDown("Jump") && grounded) {
			
			Jump();
			grounded = true;
		}

		if (curHealth > maxHealth) {
			curHealth = maxHealth;
		}
		if (curHealth < - 0) {
			Die();
		}
	}
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
	}
	
	private void LoadNewLevel () {
		Application.LoadLevel (Application.loadedLevel);
	}
	
	public void Jump() {
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}
	
	private void HandleMovement (float horizontal) {
		myRigidbody.velocity = new Vector2 (horizontal * movementSpeed, myRigidbody.velocity.y);
	}

	void Die () {
		Application.LoadLevel (Application.loadedLevel);
	}

	void onTriggerEnter2D(Collider2D col) {
		if(col.CompareTag ("Food")){
			Destroy(col.gameObject);
			gn.points += 1;
		}

	}
}
