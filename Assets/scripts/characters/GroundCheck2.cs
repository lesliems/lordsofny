﻿using UnityEngine;
using System.Collections;

public class GroundCheck2 : MonoBehaviour {
	
	private PlayerNermal PlayerNermal;
	
	// Use this for initialization
	void Start () {
		PlayerNermal = gameObject.GetComponentInParent<PlayerNermal> ();
	}
	
	void OnTriggerEnter2D(Collider2D col){
		PlayerNermal.grounded = true;
	}
	
	void OnTriggerStay2D(Collider2D col){
		PlayerNermal.grounded = true;
		
	}
	// Update is called once per frame
	void OnTriggerExit2D (Collider2D col) {
		PlayerNermal.grounded = false;
	}
}
