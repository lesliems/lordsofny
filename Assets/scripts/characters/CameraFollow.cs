﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	private Vector2 velocity;
	
	public float smoothTimeY;
	public float smoothTimeX;
	
	public GameObject PlayerBoat;
	
	public bool bounds;
	
	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;
	
	void Start () {
		PlayerBoat = GameObject.FindGameObjectWithTag ("PlayerBoat");
		
	}
	
	void FixedUpdate(){
		float posX = Mathf.SmoothDamp (transform.position.x, PlayerBoat.transform.position.x, ref velocity.x, smoothTimeX);
		float posY = Mathf.SmoothDamp (transform.position.y, PlayerBoat.transform.position.y, ref velocity.y, smoothTimeY);
		
		transform.position = new Vector3 (posX, posY, transform.position.z);
		
	}
}
	

	//SerializeField]
	//private float xMax;
	//[SerializeField]
	//private float yMax;
	//[SerializeField]
	//private float xMin;
	//[SerializeField]
	//private float yMin;

	//private Transform target;
	// Use this for initialization
	//void Start () {

	//	target = GameObject.Find ("PlayerBoat").transform;
	
	//}
	
	// Update is called once per frame
	//void LateUpdate () {
	//	transform.position = new Vector3 (Mathf.Clamp (target.position.x, xMin, xMax), Mathf.Clamp (target.position.y, yMin, yMax),transform.position.z);
	//}

