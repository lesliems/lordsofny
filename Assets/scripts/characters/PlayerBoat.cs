﻿using UnityEngine;
using System.Collections;

public class PlayerBoat : MonoBehaviour
{
	private Rigidbody2D myRigidbody;
	

	[SerializeField]
	private float movementSpeed;
	public bool grounded = true;
	public float JumpPower = 50f;
	public float jumpHeight = 10f;

	private int secondsUntilNextLevel = 1;
	
	
	void Start ()
	{
		myRigidbody = GetComponent<Rigidbody2D> ();
	}
	
	void Update ()
	{
		float horizontal = Input.GetAxis ("Horizontal");
		HandleMovement(horizontal);
		
		if (Input.GetButtonDown("Jump") && grounded) {

			Jump();
			grounded = true;
		}
		
	}
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "Exit") {
			Invoke ("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
	}

	private void LoadNewLevel () {
		Application.LoadLevel ("Level 2");
	}

	public void Jump() {
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}

	private void HandleMovement (float horizontal) {
		myRigidbody.velocity = new Vector2 (horizontal * movementSpeed, myRigidbody.velocity.y);
	}
}
