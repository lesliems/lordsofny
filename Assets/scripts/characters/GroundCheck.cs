﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

	private PlayerBoat PlayerBoat;

	// Use this for initialization
	void Start () {
		PlayerBoat = gameObject.GetComponentInParent<PlayerBoat> ();
	}

	void OnTriggerEnter2D(Collider2D col){
		PlayerBoat.grounded = true;
	}

	void OnTriggerStay2D(Collider2D col){
		PlayerBoat.grounded = true;
		
	}
	// Update is called once per frame
	void OnTriggerExit2D (Collider2D col) {
		PlayerBoat.grounded = false;
	}
}
