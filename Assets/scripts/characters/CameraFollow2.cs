﻿using UnityEngine;
using System.Collections;

public class CameraFollow2 : MonoBehaviour {
	
	private Vector2 velocity;
	
	public float smoothTimeY;
	public float smoothTimeX;
	
	public GameObject PlayerNermal;
	
	public bool bounds;
	
	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;
	
	void Start () {
		PlayerNermal = GameObject.FindGameObjectWithTag ("PlayerNermal");
		
	}
	
	void FixedUpdate(){
		float posX = Mathf.SmoothDamp (transform.position.x, PlayerNermal.transform.position.x, ref velocity.x, smoothTimeX);
		float posY = Mathf.SmoothDamp (transform.position.y, PlayerNermal.transform.position.y, ref velocity.y, smoothTimeY);
		
		transform.position = new Vector3 (posX, posY, transform.position.z);
		
	}
}